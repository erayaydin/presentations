---
author: Author Name
title: Slide Title
date: Presentation Date
revealjs-url: 'reveal.js'
theme: night
css:
  - styles/agate.css
---

## Slide 1

# Slide 2

## Slide 2.1

- Each slide begins with an `<h2>` element (`"##"` in markdown).
- Use an `<h1>` element (`"# "` in markdown) to group slides.

## Slide 2.2

Example code highlight with highlight.js

```php
// public/index.php
require __DIR__.'/../vendor/autoload.php';
```

